﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.MESManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-09 10:26
    /// 描 述：生产计划实体类
    /// </summary>
    [SugarTable("mes_plan")]
    public class PlanEntity : IEntity<PlanEntity>,ICreationAudited,IModificationAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="PlanNum", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string PlanNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="OrderNum", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string OrderNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Num", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? Num { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName= "Completiondate", ColumnDescription = "",ColumnDataType = "datetime(23)", IsNullable = true)]
        public DateTime? Completiondate { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "SortCode", ColumnDescription = "", ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }
         /// <summary>
         /// 
         /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? EnabledMark { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }

        [SugarColumn(IsIgnore = true)]
        public int? ActualNum { get; set; }
        [SugarColumn(IsIgnore = true)]
        public int? ActualNum1 { get; set; }


    }
}
