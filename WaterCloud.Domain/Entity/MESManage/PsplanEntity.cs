﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;
using WaterCloud.Code;
namespace WaterCloud.Domain.MESManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-09 13:24
    /// 描 述：排产计划实体类
    /// </summary>
    [SugarTable("mes_psplan")]
    public class PsplanEntity : IEntity<PsplanEntity>, ICreationAudited, IModificationAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "PsNum", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string PsNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "PlanNum", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string PlanNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="PlanId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string PlanId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ProcessRouteId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessRouteId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ProcessRouteName", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessRouteName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ProcessRouteVersion", ColumnDescription = "", ColumnDataType = "int(10)", IsNullable = true)]
        public int? ProcessRouteVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LineId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName= "LineCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LineName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Num", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? Num { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ActualNum", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? ActualNum { get; set; }
       
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="StartDate", ColumnDescription = "",ColumnDataType = "datetime(23)", IsNullable = true)]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EndDate", ColumnDescription = "",ColumnDataType = "datetime(23)", IsNullable = true)]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }

      

    }
}
