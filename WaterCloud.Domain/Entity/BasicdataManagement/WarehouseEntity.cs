﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 11:04
    /// 描 述：仓库实体类
    /// </summary>
    [SugarTable("basic_warehouse")]
    public class WarehouseEntity : IEntity<WarehouseEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",IsPrimaryKey = true)]
        public string Id { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string? WarehousetypeName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Warehousetype", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? Warehousetype { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="WarehousetypeId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? WarehousetypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "WarehouseName", ColumnDescription = "", ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? WarehouseName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ErpName", ColumnDescription = "", ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ErpName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ErpCode", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ErpCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "IsGoods", ColumnDescription = "", ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? IsGoods { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "StorageWay", ColumnDescription = "", ColumnDataType = "int(4)", IsNullable = true)]
        public int? StorageWay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string? Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? DeleteUserId { get; set; }
        [SugarColumn(IsIgnore = true)]
        //tablecheck字段
        public bool LAY_CHECKED { get; set; }
    }
}
