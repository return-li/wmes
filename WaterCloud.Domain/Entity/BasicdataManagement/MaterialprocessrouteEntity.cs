﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-13 11:51
    /// 描 述：分配工艺路线实体类
    /// </summary>
    [SugarTable("basic_materialprocessroute")]
    public class MaterialprocessrouteEntity : IEntity<MaterialprocessrouteEntity>, ICreationAudited, IModificationAudited
    
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcessRouteId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessRouteId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcessRouteName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessRouteName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcessRouteVersion", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? ProcessRouteVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "DeleteMark", ColumnDescription = "", ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "EnabledMark", ColumnDescription = "", ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "Description", ColumnDescription = "", ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "CreatorTime", ColumnDescription = "", ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "CreatorUserId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "CreatorUserName", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "LastModifyTime", ColumnDescription = "", ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "LastModifyUserId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "DeleteTime", ColumnDescription = "", ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "DeleteUserId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string DeleteUserId { get; set; }
        [SugarColumn(IsIgnore = true)]
        //tablecheck字段
        public bool LAY_CHECKED { get; set; }
    }
}
