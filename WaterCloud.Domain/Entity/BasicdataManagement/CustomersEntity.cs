﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-28 15:41
    /// 描 述：客户实体类
    /// </summary>
    [SugarTable("basic_customers")]
    public class CustomersEntity : IEntity<CustomersEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(100)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ErpEnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? ErpEnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CustomerName", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? CustomerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string? Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? DeleteUserId { get; set; }

        [SugarColumn(IsIgnore = true)]
        //tablecheck字段
        public bool LAY_CHECKED { get; set; }
    }
}
