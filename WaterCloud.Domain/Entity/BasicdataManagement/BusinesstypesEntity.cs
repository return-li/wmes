﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-01-07 10:48
    /// 描 述：业务类型实体类
    /// </summary>
    [SugarTable("basic_businesstypes")]
    public class BusinesstypesEntity : IEntity<BusinesstypesEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(100)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="BusinessTypeName", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? BusinessTypeName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "DocumentstypeId", ColumnDescription = "", ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? DocumentstypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="WarehouseId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? WarehouseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SendReceiveId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? SendReceiveId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="OrgId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? OrgId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="UserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CustomerId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CustomerId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SupplierId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? SupplierId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SalesTypeId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? SalesTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcurementTypeId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ProcurementTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="BusinesUser", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? BusinesUser { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="BusinesssType", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? BusinesssType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ErpAccount", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ErpAccount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="IsChecked", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? IsChecked { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string? Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName= "CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? DeleteUserId { get; set; }
      
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn( ColumnName = "Input", ColumnDescription = "", ColumnDataType = "longtext nvarchar(-1)", IsNullable = true)]
        public string? Input { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "Checked", ColumnDescription = "", ColumnDataType = "longtext nvarchar(-1)", IsNullable = true)]
        public string? Checked { get; set; }
        //多选显示字段
        [SugarColumn(IsIgnore = true)]
        public string InputRoleName { get; set; }
        [SugarColumn(IsIgnore = true)]
        //多选显示字段
        public string CheckedRoleName { get; set; }
        
    }
}
