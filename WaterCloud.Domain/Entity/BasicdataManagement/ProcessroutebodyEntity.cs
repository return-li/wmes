﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-05 13:09
    /// 描 述：工艺路线记录实体类
    /// </summary>
    [SugarTable("basic_processroutebody")]
    public class ProcessroutebodyEntity : IEntity<ProcessroutebodyEntity>,ICreationAudited,IModificationAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="HeadId", ColumnDescription = "",ColumnDataType = "nvarchar(50)")]
        public string HeadId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "LineId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineId{ get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LineCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LineName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LineName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcessCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProcessName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ProcessId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProcessId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }
    }
}
