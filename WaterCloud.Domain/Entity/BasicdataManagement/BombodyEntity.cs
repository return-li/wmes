﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-08 14:16
    /// 描 述：BOM记录实体类
    /// </summary>
    [SugarTable("basic_bombody")]
    public class BombodyEntity : IEntity<BombodyEntity>,ICreationAudited,IModificationAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "MaterialId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="MaterialName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string MaterialName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="HeadId", ColumnDescription = "",ColumnDataType = "nvarchar(50)")]
        public string HeadId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Unit", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string Unit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Num", ColumnDescription = "",ColumnDataType = "float(53)", IsNullable = true)]
        public float? Num { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }
    }
}
