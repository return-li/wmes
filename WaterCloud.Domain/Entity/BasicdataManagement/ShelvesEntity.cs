﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 15:27
    /// 描 述：货架管理实体类
    /// </summary>
    [SugarTable("basic_shelves")]
    public class ShelvesEntity : IEntity<ShelvesEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string? EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ShelvesName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ShelvesName { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string? ShelvestypeName { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string? AreaName { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string? WarehouseName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "AreaId", ColumnDescription = "", ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? AreaId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "WarehouseId", ColumnDescription = "", ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? WarehouseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ShelvestypeId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? ShelvestypeId { get; set; }

        

            [SugarColumn(ColumnName = "Way", ColumnDescription = "", ColumnDataType = "int(4)", IsNullable = true)]
        public int? Way { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        [SugarColumn(ColumnName = "Num", ColumnDescription = "", ColumnDataType = "int(4)", IsNullable = true)]
        public int? Num { get; set; }

        [SugarColumn(ColumnName = "FloorNum", ColumnDescription = "", ColumnDataType = "int(4)", IsNullable = true)]
        public int? FloorNum { get; set; }

        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(4)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(1)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string? Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(8)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string? DeleteUserId { get; set; }
    }
}
