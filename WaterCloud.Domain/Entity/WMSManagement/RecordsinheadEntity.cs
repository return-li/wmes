﻿using System;
using System.ComponentModel.DataAnnotations;
using SqlSugar;

namespace WaterCloud.Domain.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-19 09:35
    /// 描 述：入库单实体类
    /// </summary>
    [SugarTable("wms_recordsinhead")]
    public class RecordsinheadEntity : IEntity<RecordsinheadEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName= "RecodNum", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string RecodNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="RecodTypeId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string RecodTypeId { get; set; }
        /// <summary>
        /// 多选显示字段
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string RecodTypeName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName= "RecodDate", ColumnDescription = "",ColumnDataType = "date(10)", IsNullable = true)]
        public DateTime? RecodDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="WarehouseId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string WarehouseId { get; set; }
        /// <summary>
        /// 多选显示字段
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string WarehouseName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="OrderNum", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string OrderNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SupplierId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string SupplierId { get; set; }
        /// <summary>
        /// 多选显示字段
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string SupplierName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SendReceiveId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string SendReceiveId { get; set; }
        /// <summary>
        /// 多选显示字段
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string SendReceiveName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string DeleteUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string AuditUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string AuditUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? AuditTime { get; set; }
    }
}
