﻿using System;
using System.ComponentModel.DataAnnotations;
using RabbitMQ.Client;
using SqlSugar;
using WaterCloud.Code;

namespace WaterCloud.Domain.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-19 15:26
    /// 描 述：入库单明细实体类
    /// </summary>
    [SugarTable("wms_recordinbody")]
    [RabbitMq(
    Queue = "InWarehouseAdd",
    Exchange = "InWarehouseAddexchange",
    ExchangeType = "fanout",
    RoutingKey = "InWarehouseAdd",
    MaximumPriority = 10,
    RetryCount = 2,
    LazyMode = "lazy",
    DeadLetter = true,
    DeadLetterExchange = "InWarehouseAdd.dle",
    DeadLetterRoutingKey = "InWarehouseAdd.dle.key"
    )]
    public class RecordinbodyEntity : IEntity<RecordinbodyEntity>,ICreationAudited,IModificationAudited,IDeleteAudited
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Id", ColumnDescription = "",ColumnDataType = "nvarchar(50)",IsPrimaryKey = true)]
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="HeadId", ColumnDescription = "",ColumnDataType = "nvarchar(50)")]
        public string HeadId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "LoadAreaCode", ColumnDescription = "", ColumnDataType = "varchar(50)", IsNullable = true)]
        public string LoadAreaCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnCode", ColumnDescription = "",ColumnDataType = "varchar(50)", IsNullable = true)]
        public string EnCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="WarehouseId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string WarehouseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "AreaId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string AreaId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "ShelvesId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ShelvesId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "GoodsId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string GoodsId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProductCode", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string ProductCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="ProductName", ColumnDescription = "",ColumnDataType = "nvarchar(100)", IsNullable = true)]
        public string ProductName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Cinvstd", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string Cinvstd { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Batch", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string Batch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Number", ColumnDescription = "",ColumnDataType = "float(53)", IsNullable = true)]
        public float? Number { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Number1", ColumnDescription = "",ColumnDataType = "float(53)", IsNullable = true)]
        public float? Number1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CinvmUnit", ColumnDescription = "",ColumnDataType = "nvarchar(20)", IsNullable = true)]
        public string CinvmUnit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CinvmUnit1", ColumnDescription = "",ColumnDataType = "nvarchar(20)", IsNullable = true)]
        public string CinvmUnit1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="SortCode", ColumnDescription = "",ColumnDataType = "int(10)", IsNullable = true)]
        public int? SortCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "State", ColumnDescription = "", ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName = "OutRecordId", ColumnDescription = "", ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string OutRecordId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="EnabledMark", ColumnDescription = "",ColumnDataType = "tinyint(3)", IsNullable = true)]
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="Description", ColumnDescription = "",ColumnDataType = "nvarchar(-1)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="CreatorUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="LastModifyUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="DeleteUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string DeleteUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditUserId", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string AuditUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditUserName", ColumnDescription = "",ColumnDataType = "nvarchar(50)", IsNullable = true)]
        public string AuditUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(ColumnName="AuditTime", ColumnDescription = "",ColumnDataType = "datetime2(27)", IsNullable = true)]
        public DateTime? AuditTime { get; set; }

        
       [SugarColumn(IsIgnore = true)]
        public string WarehouseName { get; set; }
        
        [SugarColumn(IsIgnore = true)]
        public string AreaName { get; set; }
       
        [SugarColumn(IsIgnore = true)]
        public string ShelvesName { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string GoodsCode { get; set; }

        [SugarColumn(IsIgnore = true)]
        public string companyId { get; set; }

        [SugarColumn(IsIgnore = true)]
        public int? StorageWay { get; set; }
        [SugarColumn(IsIgnore = true)]
        public string RecodNum { get; set; }

        [SugarColumn(IsIgnore = true)]
        public int? Tag { get; set; }
    }
}
