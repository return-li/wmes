﻿using System;
using System.ComponentModel.DataAnnotations;
using RabbitMQ.Client;
using SqlSugar;
using WaterCloud.Code;

namespace WaterCloud.Domain.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-19 15:26
    /// 描 述：库存实体类
    /// </summary>
 
    public class ReportEntity 
    {
        public string Id { get; set; }
        
        public string HeadId { get; set; }

        public string LoadAreaCode { get; set; }
       
        public string EnCode { get; set; }
       
     
        public string WarehouseId { get; set; }
      
        public string AreaId { get; set; }
      
        public string ShelvesId { get; set; }
     
        public string GoodsId { get; set; }
   
        public string ProductCode { get; set; }
    
        public string ProductName { get; set; }
      
        public string Cinvstd { get; set; }
     
        public string Batch { get; set; }
   
        public float? Number { get; set; }
     
        public float? Number1 { get; set; }
      
        public string CinvmUnit { get; set; }
    
        public string CinvmUnit1 { get; set; }

   
        public string WarehouseName { get; set; }
        
       
        public string AreaName { get; set; }
       
       
        public string ShelvesName { get; set; }

  
        public string GoodsCode { get; set; }

      
        public string companyId { get; set; }

     
        public int? StorageWay { get; set; }
       
        public string RecodNum { get; set; }

      
        public int? Tag { get; set; }
    }
}
