﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.MESManage;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.MESManage;
using WaterCloud.Service.SystemOrganize;

namespace WaterCloud.Web.Areas.MESManage.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-09 10:26
    /// 描 述：生产计划控制器类
    /// </summary>
    [Area("MESManage")]
    public class PlanController :  ControllerBase
    {
        public PlanService _service {get;set;}
 
        #region 获取数据
        [HttpGet]
        public virtual ActionResult PsIndex()
        {
            return View();
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<PlanEntity> pagination, string keyword)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "desc";
            }
            var data = await _service.GetLookList(pagination, keyword);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var d =await _service.GetListSum(keyword);
                    data[i].ActualNum = d[0].ActualNum ;
                    data[i].ActualNum1 = d[0].ActualNum1;
                }

                    
                
            }
        
            return Content(pagination.setData(data).ToJson());
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJsonRun(SoulPage<PlanEntity> pagination, string keyword)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "desc";
            }
            var data = await _service.GetLookListRun(pagination, keyword);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    var d = await _service.GetListSum(keyword);
                    data[i].ActualNum = d[0].ActualNum;
                    data[i].ActualNum1 = d[0].ActualNum1;
                }



            }
            return Content(pagination.setData(data).ToJson());
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);
            return Content(data.ToJson());
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(PlanEntity entity, string keyValue)
        {
            try
            {
                await _service.SubmitForm(entity, keyValue);
                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitFormState( string keyValue,int state)
        {
            try
            {
                await _service.SubmitFormState( state,keyValue);
                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }
        [HttpPost]
        [HandlerAjaxOnly]
        [HandlerAuthorize]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {
                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
