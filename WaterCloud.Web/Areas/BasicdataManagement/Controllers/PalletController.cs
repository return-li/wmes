﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.BasicdataManagement;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.BasicdataManagement;

namespace WaterCloud.Web.Areas.BasicdataManagement.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 13:20
    /// 描 述：托盘管理控制器类
    /// </summary>
    [Area("BasicdataManagement")]
    public class PalletController :  ControllerBase
    {
        public PalletService _service {get;set;}

        #region 获取数据
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<PalletEntity> pagination, string keyword)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "asc";
            }
            var data = await _service.GetLookList(pagination,keyword);
            return Content(pagination.setData(data).ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);
            return Content(data.ToJson());
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(PalletEntity entity, string keyValue)
        {
            try
            {
               string r= await _service.SubmitForm(entity, keyValue);
                if (r == "ok") { return await Success("操作成功。", "", ""); } else { return await Error( "编码重复，未添加！", "", ""); }

            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }

        public virtual ActionResult Formbatch()

        {
            return View();
        }
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitFormBath(PalletEntity entity)
        {
            try
            {
                

                    string r=await _service.SubmitFormBatch(entity);
                if (r == "") { return await Success("操作成功。", "", ""); } else { return await Success(r+"编码重复，未添加！", "", ""); }

                    
               
                
                
            }
            catch (Exception ex)
            {
                return await Error(ex.Message,"","");
            }
        }
        [HttpPost]
        [HandlerAjaxOnly]
        [ServiceFilter(typeof(HandlerAuthorizeAttribute))]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {
                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
