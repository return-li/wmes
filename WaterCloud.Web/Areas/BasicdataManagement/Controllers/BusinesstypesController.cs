﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.BasicdataManagement;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.BasicdataManagement;
using WaterCloud.Service.SystemOrganize;
namespace WaterCloud.Web.Areas.BasicdataManagement.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-01-07 10:48
    /// 描 述：业务类型控制器类
    /// </summary>
    [Area("BasicdataManagement")]
    public class BusinesstypesController :  ControllerBase
    {
        public RoleService _roleService { get; set; }
        public BusinesstypesService _service {get;set;}

        #region 获取数据
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<BusinesstypesEntity> pagination, string keyword)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "asc";
            }
            var data = await _service.GetLookList(pagination,keyword);
            return Content(pagination.setData(data).ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);
            if (!string.IsNullOrEmpty(data.Input))
            {
                List<string> str = new List<string>();
                foreach (var item in data.Input.Split(','))
                {
                    var temp = await _roleService.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.FullName);
                    }
                }
                data.InputRoleName = string.Join("  ", str.ToArray());
            }
            if (!string.IsNullOrEmpty(data.Checked))
            {
                List<string> str = new List<string>();
                foreach (var item in data.Checked.Split(','))
                {
                    var temp = await _roleService.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.FullName);
                    }
                }
                data.CheckedRoleName = string.Join("  ", str.ToArray());
            }
            return Content(data.ToJson());
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(BusinesstypesEntity entity, string keyValue)
        {
            try
            {
                await _service.SubmitForm(entity, keyValue);
                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }

        [HttpPost]
        [HandlerAjaxOnly]
        [ServiceFilter(typeof(HandlerAuthorizeAttribute))]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {
                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
