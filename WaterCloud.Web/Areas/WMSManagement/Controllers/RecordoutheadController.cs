﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.WMSManagement;
using WaterCloud.Service.BasicdataManagement;

namespace WaterCloud.Web.Areas.WMSManagement.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-23 13:32
    /// 描 述：出库单控制器类
    /// </summary>
    [Area("WMSManagement")]
    public class RecordoutheadController :  ControllerBase
    {
        public RecordoutheadService _service {get;set;}
  
        public WarehouseService _wareservice { get; set; }
        public SendreceiveService _srservice { get; set; }
        public CustomersService _cusservice { get; set; }
        public DocumentstypeService _doctypeservice { get; set; }
        #region 获取数据
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<RecordoutheadEntity> pagination, string keyword)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "desc";
            }
            var data = await _service.GetLookList(pagination, keyword);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].WarehouseId.Split(','))
                        {
                            var temp = await _wareservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.WarehouseName);
                            }
                        }
                        data[i].WarehouseName = string.Join("  ", str.ToArray());
                    }

                    if (!string.IsNullOrEmpty(data[i].SendReceiveId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].SendReceiveId.Split(','))
                        {
                            var temp = await _srservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.SendReceiveName);
                            }
                        }
                        data[i].SendReceiveName = string.Join("  ", str.ToArray());
                    }

                    if (!string.IsNullOrEmpty(data[i].RecodTypeId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].RecodTypeId.Split(','))
                        {
                            var temp = await _doctypeservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.DocumentstypeName);
                            }
                        }
                        data[i].RecodTypeName = string.Join("  ", str.ToArray());
                    }

                    if (!string.IsNullOrEmpty(data[i].CustomerId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].CustomerId.Split(','))
                        {
                            var temp = await _cusservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.CustomerName);
                            }
                        }
                        data[i].CustomerName = string.Join("  ", str.ToArray());
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);
         
            if (!string.IsNullOrEmpty(data.WarehouseId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.WarehouseId.Split(','))
                {
                    var temp = await _wareservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.WarehouseName);
                    }
                }
                data.WarehouseName = string.Join("  ", str.ToArray());
            }

            if (!string.IsNullOrEmpty(data.SendReceiveId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.SendReceiveId.Split(','))
                {
                    var temp = await _srservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.SendReceiveName);
                    }
                }
                data.SendReceiveName = string.Join("  ", str.ToArray());
            }

            if (!string.IsNullOrEmpty(data.RecodTypeId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.RecodTypeId.Split(','))
                {
                    var temp = await _doctypeservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.DocumentstypeName);
                    }
                }
                data.RecodTypeName = string.Join("  ", str.ToArray());
            }

            if (!string.IsNullOrEmpty(data.CustomerId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.CustomerId.Split(','))
                {
                    var temp = await _cusservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.CustomerName);
                    }
                }
                data.CustomerName = string.Join("  ", str.ToArray());
            }



            return Content(data.ToJson());
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(RecordoutheadEntity entity, string keyValue)
        {
            try
            {
                await _service.SubmitForm(entity, keyValue);
                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }

        [HttpPost]
        [HandlerAjaxOnly]
        [HandlerAuthorize]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {
                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
