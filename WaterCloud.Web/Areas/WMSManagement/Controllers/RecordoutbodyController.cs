﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.WMSManagement;
using WaterCloud.Service.BasicdataManagement;
using WaterCloud.Domain.BasicdataManagement;
using Serenity.ComponentModel;

namespace WaterCloud.Web.Areas.WMSManagement.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-23 13:34
    /// 描 述：出库单明细控制器类
    /// </summary>
    [Area("WMSManagement")]
    public class RecordoutbodyController :  ControllerBase
    {
        public RecordoutbodyService _service {get;set;}
        public RecordinbodyService _inservice { get; set; }
        public GoodsService _goodsservice { get; set; }
        public StorageAreaService _areaservice { get; set; }
        public ShelvesService _shelvesservice { get; set; }
        public WarehouseService _wareservice { get; set; }
        public RecordoutheadService _roservice { get; set; }
        #region 获取数据
        [HttpGet]
        public ActionResult DownIndex()
        {

            return View();
        }
        [HttpGet]
        public ActionResult InIndex()
        {

            return View();
        }
        [HttpGet]
        public ActionResult list()
        {

            return View();
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<RecordoutbodyEntity> pagination, string keyword, string hid)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "desc";
            }
            var data = await _service.GetLookList(pagination, keyword, hid);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {

                        var temp = await _wareservice.GetForm(data[i].WarehouseId);
                        data[i].WarehouseName = temp.WarehouseName;
                        data[i].StorageWay = temp.StorageWay;
                    }


                    if (!string.IsNullOrEmpty(data[i].AreaId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].AreaId.Split(','))
                        {
                            var temp = await _areaservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.AreaName);
                            }
                        }
                        data[i].AreaName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].ShelvesId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].ShelvesId.Split(','))
                        {
                            var temp = await _shelvesservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.ShelvesName);
                            }
                        }
                        data[i].ShelvesName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].GoodsId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].GoodsId.Split(','))
                        {
                            var temp = await _goodsservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.EnCode);
                            }
                        }
                        data[i].GoodsCode = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].HeadId))
                    {
                        var d = await _roservice.GetLookListid(data[i].HeadId);
                        data[i].RecodNum = d[0].RecodNum;
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }
        
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJsonDown(SoulPage<RecordoutbodyEntity> pagination)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "HeadId,Id";
                pagination.order = "asc";
            }
            var data = await _service.GetLookListDown(pagination);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {

                        var temp = await _wareservice.GetForm(data[i].WarehouseId);
                        data[i].WarehouseName = temp.WarehouseName;
                        data[i].StorageWay = temp.StorageWay;
                    }


                    if (!string.IsNullOrEmpty(data[i].AreaId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].AreaId.Split(','))
                        {
                            var temp = await _areaservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.AreaName);
                            }
                        }
                        data[i].AreaName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].ShelvesId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].ShelvesId.Split(','))
                        {
                            var temp = await _shelvesservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.ShelvesName);
                            }
                        }
                        data[i].ShelvesName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].GoodsId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].GoodsId.Split(','))
                        {
                            var temp = await _goodsservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.EnCode);
                            }
                        }
                        data[i].GoodsCode = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].HeadId))
                    {
                        var d = await _roservice.GetLookListid(data[i].HeadId);
                        data[i].RecodNum = d[0].RecodNum;
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListIsNull(string keyword)
        {
            var data = await _service.GetListIsNull(keyword);
            return Content(data.ToJson());
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);
            return Content(data.ToJson());
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJsonUp(SoulPage<RecordinbodyEntity> pagination, string WarehouseId, string keyword)

        {

            var data = await _inservice.GetLookListUp( pagination, WarehouseId, keyword);

            return Content(data.ToJson());


        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(string keyValue, string hid, string wid)
        {
            try
            {

                await _service.SubmitForm(keyValue, wid,hid);
                return await Success("操作成功。", "",hid);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", hid);
            }
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> Down(RecordoutbodyEntity entity, string keyValue,string GoodsId)
        {
            try
            {
                await _service.Down(entity, keyValue, GoodsId);

                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }
        [HttpPost]
        [HandlerAjaxOnly]
        [HandlerAuthorize]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {
                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
