﻿using Autofac.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCloud.Code;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Service.BasicdataManagement;
using WaterCloud.Service.SystemManage;
using WaterCloud.Service.WMSManagement;

namespace WaterCloud.Web.Areas.WMSManagement.Controllers
{ /// <summary>
  /// 创 建：超级管理员
  /// 日 期：2022-08-23 13:34
  /// 描 述：库存报表控制器类
  /// </summary>
    [Area("WMSManagement")]
    public class ReportController : ControllerBase
    {
        public RePortService _service { get; set; }
        public RecordinbodyService _rservice { get; set; }

        [HttpGet]
        public ActionResult KucunReport()
        {

            return View();
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<RecordinbodyEntity> pagination, string keyword, string hid)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "ProductCode";
                pagination.order = "asc";
            }

            var data = await _service.GetLookList(pagination, keyword, hid);
           
            return Content(pagination.setData(data).ToJson());
        }
    }
}
