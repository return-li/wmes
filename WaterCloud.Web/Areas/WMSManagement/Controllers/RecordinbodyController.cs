﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WaterCloud.Code;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Service;
using Microsoft.AspNetCore.Authorization;
using WaterCloud.Service.WMSManagement;
using WaterCloud.Service.BasicdataManagement;
using static Serenity.Web.PropertyItemsScript;
using WaterCloud.Service.SystemManage;
using WaterCloud.Domain.BasicdataManagement;
using WaterCloud.Service.SystemOrganize;

namespace WaterCloud.Web.Areas.WMSManagement.Controllers
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-19 15:26
    /// 描 述：入库单明细控制器类
    /// </summary>
    [Area("WMSManagement")]
    public class RecordinbodyController :  ControllerBase
    {
        public RecordinbodyService _service {get;set;}
        public GoodsService _goodsservice { get; set; }
        public StorageAreaService _areaservice { get; set; }
        public ShelvesService _shelvesservice { get; set; }
        public WarehouseService _wareservice { get; set; }
        public RecordsinheadService _riservice { get; set; }
        #region 获取数据

        [HttpGet]
        public ActionResult UpIndex()
        {

            return View();
        }
        [HttpGet]
        public ActionResult UpForm()
        {

            return View();
        }
        [HttpGet]
        public ActionResult list()
        {

            return View();
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJson(SoulPage<RecordinbodyEntity> pagination, string keyword, string hid)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "asc";
            }


            var data = await _service.GetLookList(pagination, keyword, hid);
            if (data.Count > 0)
            {


                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {

                        var temp = await _wareservice.GetForm(data[i].WarehouseId);
                        data[i].WarehouseName =temp.WarehouseName;
                        data[i].StorageWay = temp.StorageWay;
                    }
               

                    if (!string.IsNullOrEmpty(data[i].AreaId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].AreaId.Split(','))
                        {
                            var temp = await _areaservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.AreaName);
                            }
                        }
                        data[i].AreaName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].ShelvesId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].ShelvesId.Split(','))
                        {
                            var temp = await _shelvesservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.ShelvesName);
                            }
                        }
                        data[i].ShelvesName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].GoodsId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].GoodsId.Split(','))
                        {
                            var temp = await _goodsservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.EnCode);
                            }
                        }
                        data[i].GoodsCode = string.Join("  ", str.ToArray());
                    }

                    if (!string.IsNullOrEmpty(data[i].HeadId))
                    {
                        var d = await _riservice.GetLookListid(data[i].HeadId);
                        data[i].RecodNum = d[0].RecodNum;
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJsonOut(SoulPage<RecordinbodyEntity> pagination, string keyword, string Warehouseid)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "Id";
                pagination.order = "asc";
            }
            var data = await _service.GetLookListOut(pagination, keyword, Warehouseid);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {

                        var temp = await _wareservice.GetForm(data[i].WarehouseId);
                        data[i].WarehouseName = temp.WarehouseName;
                        data[i].StorageWay = temp.StorageWay;
                    }


                    if (!string.IsNullOrEmpty(data[i].AreaId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].AreaId.Split(','))
                        {
                            var temp = await _areaservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.AreaName);
                            }
                        }
                        data[i].AreaName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].ShelvesId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].ShelvesId.Split(','))
                        {
                            var temp = await _shelvesservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.ShelvesName);
                            }
                        }
                        data[i].ShelvesName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].GoodsId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].GoodsId.Split(','))
                        {
                            var temp = await _goodsservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.EnCode);
                            }
                        }
                        data[i].GoodsCode = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].HeadId))
                    {
                        var d = await _riservice.GetLookListid(data[i].HeadId);
                        data[i].RecodNum = d[0].RecodNum;
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }
        [HandlerAjaxOnly]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult> GetGridJsonUp(SoulPage<RecordinbodyEntity> pagination)
        {
            if (string.IsNullOrEmpty(pagination.field))
            {
                pagination.field = "HeadId,Id";
                pagination.order = "asc";
            }
            var data = await _service.GetLookListUp(pagination);
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(data[i].WarehouseId))
                    {

                        var temp = await _wareservice.GetForm(data[i].WarehouseId);
                        data[i].WarehouseName = temp.WarehouseName;
                        data[i].StorageWay = temp.StorageWay;
                    }

                    if (!string.IsNullOrEmpty(data[i].AreaId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].AreaId.Split(','))
                        {
                            var temp = await _areaservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.AreaName);
                            }
                        }
                        data[i].AreaName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].ShelvesId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].ShelvesId.Split(','))
                        {
                            var temp = await _shelvesservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.ShelvesName);
                            }
                        }
                        data[i].ShelvesName = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].GoodsId))
                    {
                        List<string> str = new List<string>();
                        foreach (var item in data[i].GoodsId.Split(','))
                        {
                            var temp = await _goodsservice.GetForm(item);
                            if (temp != null)
                            {
                                str.Add(temp.EnCode);
                            }
                        }
                        data[i].GoodsCode = string.Join("  ", str.ToArray());
                    }
                    if (!string.IsNullOrEmpty(data[i].HeadId))
                    {
                        var d = await _riservice.GetLookListid(data[i].HeadId);
                        data[i].RecodNum = d[0].RecodNum;
                    }

                }
            }
            return Content(pagination.setData(data).ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListIsNull(string keyword)
        {
            var data = await _service.GetListIsNull(keyword);
            return Content(data.ToJson());
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJson(string keyword)
        {
            var data = await _service.GetList(keyword);
            return Content(data.ToJson());
        }

        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetFormJson(string keyValue)
        {
            var data = await _service.GetLookForm(keyValue);

            if (!string.IsNullOrEmpty(data.WarehouseId))
            {
                var temp = await _wareservice.GetForm(data.WarehouseId);
                data.WarehouseName = temp.WarehouseName;
                data.StorageWay = temp.StorageWay;
            }


            if (!string.IsNullOrEmpty(data.AreaId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.AreaId.Split(','))
                {
                    var temp = await _areaservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.AreaName);
                    }
                }
                data.AreaName = string.Join("  ", str.ToArray());
            }
            if (!string.IsNullOrEmpty(data.ShelvesId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.ShelvesId.Split(','))
                {
                    var temp = await _shelvesservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.ShelvesName);
                    }
                }
                data.ShelvesName = string.Join("  ", str.ToArray());
            }
            if (!string.IsNullOrEmpty(data.GoodsId))
            {
                List<string> str = new List<string>();
                foreach (var item in data.GoodsId.Split(','))
                {
                    var temp = await _goodsservice.GetForm(item);
                    if (temp != null)
                    {
                        str.Add(temp.EnCode);
                    }
                }
                data.GoodsCode = string.Join("  ", str.ToArray());
            }
            if (!string.IsNullOrEmpty(data.HeadId))
            {
                var d = await _riservice.GetLookListid(data.HeadId);
                data.RecodNum = d[0].RecodNum;
            }
            return Content(data.ToJson());
        }
        [HttpGet]
        [HandlerAjaxOnly]
        public async Task<ActionResult> GetListJsonUp(string WarehouseId, string keyword)

        {

            GoodsEntity data = new GoodsEntity();
            var dataed = await _service.GetListuped(WarehouseId, keyword);
            var a = false;

            if (dataed.Count > 0)
            {

                foreach (var item in dataed)
                {

                    var datalocation = await _goodsservice.GetListlocation(item.ShelvesId);
                    for (int i = 0; i < datalocation.Count; i++)
                    {

                        if (datalocation[i].Id == item.GoodsId)
                        {

                            for (int up = i; up > 0; up--)
                            {
                                var d = await _goodsservice.GetListlocationIsNull(datalocation[up].Id);
                                if (d.Count > 0)
                                {

                                    data.Id = datalocation[up].Id;
                                    data.ShelvesId = datalocation[up].ShelvesId;
                                    data.AreaId = datalocation[up].AreaId;
                                    a = true;
                                    break;

                                }

                            }
                            for (int down = i; down < datalocation.Count; down++)
                            {
                                var d = await _goodsservice.GetListlocationIsNull(datalocation[down].Id);
                                if (d.Count > 0)
                                {

                                    data.Id = datalocation[down].Id;
                                    data.ShelvesId = datalocation[down].ShelvesId;
                                    data.AreaId = datalocation[down].AreaId;
                                    a = true;
                                    break;

                                }

                            }


                        }

                        if (a)
                        {
                            break;
                        }

                    }
                    if (a)
                    {
                        break;
                    }
                }

            }
            else
            {

                var dataarea = await _areaservice.GetListarea(WarehouseId);
                foreach (var area in dataarea)
                {

                    var datashelves = await _shelvesservice.GetListShelves(area.Id);
                    foreach (var shelves in datashelves)
                    {
                        var datalocation = await _goodsservice.GetListlocation(shelves.Id);
                        foreach (var location in datalocation)
                        {
                            var d = await _goodsservice.GetListlocationIsNull(location.Id);
                            if (d.Count > 0)
                            {

                                data.Id = location.Id;
                                data.ShelvesId = location.ShelvesId;
                                data.AreaId = location.AreaId;
                                a = true;
                                break;

                            }

                        }
                        if (a)
                        {
                            break;
                        }
                    }
                    if (a)
                    {
                        break;
                    }
                }
            }

            return Content(data.ToJson());


        }
        #endregion

        #region 提交数据
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> SubmitForm(RecordinbodyEntity entity, string keyValue,string oldgid)
        {
            try
            {
                
                await _service.SubmitForm(entity, keyValue,oldgid);

                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }
        [HttpPost]
        [HandlerAjaxOnly]
        public async Task<ActionResult> Up(RecordinbodyEntity entity, string keyValue)
        {
            try
            {
                await _service.Up(entity, keyValue);

                return await Success("操作成功。", "", keyValue);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue);
            }
        }
        [HttpPost]
        [HandlerAjaxOnly]
        [HandlerAuthorize]
        public async Task<ActionResult> DeleteForm(string keyValue)
        {
            try
            {

                await _service.DeleteForm(keyValue);
                return await Success("操作成功。", "", keyValue, DbLogType.Delete);
            }
            catch (Exception ex)
            {
                return await Error(ex.Message, "", keyValue, DbLogType.Delete);
            }
        }
        #endregion
    }
}
