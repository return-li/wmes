<p></p>
<p></p>



----
# WMES

#### 功能模块介绍
涵盖WMS\MES基础功能模块。

 用户管理
 部门管理
 角色管理
 数据字典
 物料管理
 客户管理
 供应商管理
 承运商管理
 仓库管理
 库区管理
 货架管理
 入库管理
 出库管理
 库存查询
 库存记录
 工艺管理
 BOM管理
 生产计划
 排产计划

 系统日志
 登陆统计
![输入图片说明](lct.png)
![输入图片说明](lct2.png)
![输入图片说明](1.png)
![输入图片说明](2.png)
![输入图片说明](3.png)
#### 前端以及后端使用技术介绍

1、前端技术

- js框架：jquery-3.4.1、LayUI、LayUI mini（开源）。
- 图标：Font Awesome 4.7.0及LayUI自带。
- 客户端验证：LayUI verify。
- 富文本编辑器：开源wangEditor、LayUI editor。
- 上传文件：LayUI upoload。
- 动态页签：LayUI mini miniTab。
- 数据表格：LayUI table、LayUI 开源 soul-table组件(已实现后端筛选)。
- 下拉选择框：LayUI select、xmselect。
- 树结构控件：LayUI 开源 dtree。
- 树状表格：LayUI 开源 treetable-lay,框架改造treetable低版本(兼容soul-table组件、修复固定列等BUG)。
- 穿梭框：LayUI transfer。
- 页面布局：LayUI、LayUI mini。
- 图表插件：echarts
- 日期控件：LayUI laydate
- 图标选择：LayUI 开源 IconPicker
- 省市区选择：LayUI 开源 layarea

2、后端技术

- 核心框架：ASP.NET 6.0、WEB API
- 定时任务：QuartZ，实现web控制
- 持久层框架：SqlSugar（支持多种数据库，复杂查询操作、多租户、分库分表等）、Chloe(支持多种数据库，复杂查询操作，比较稳定)
- 安全支持：过滤器、Sql注入、请求伪造
- 服务端验证：实体模型验证
- 缓存框架：Redis/Memory（单点登录控制）
- 消息队列: RabbitMq
- 日志管理：Log、登录日志、操作日志
- 工具类：NPOI、Newtonsoft.Json、验证码、丰富公共类
- 其他：AutoFac、Swagger


#### 环境要求

1. VS2022及以上版本；
2. Asp.net 6.0；
3. Mysql或者SQLSERVER2005及以上版本，database文件夹下有sql文件可执行；
4. 请使用VS2022及以上版本打开解决方案。
5. Redis和RabbitMq在项目文件夹里有

![输入图片说明](bzh%E7%BE%A4%E4%BA%8C%E7%BB%B4%E7%A0%81.png)
