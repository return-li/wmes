﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.MESManage;
using WaterCloud.Domain.BasicdataManagement;
using NPOI.SS.Formula.Functions;
using Serenity.Services;

namespace WaterCloud.Service.MESManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-09 10:26
    /// 描 述：生产计划服务类
    /// </summary>
    public class PlanService : DataFilterService<PlanEntity>, IDenpendency
    {
        public PlanService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<PlanEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }
        public async Task<List<PlanEntity>> GetListSum(string keyword = "")
        {
            var data = repository.Db.Queryable<PsplanEntity>().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.PlanId.Contains(keyword));
            }

          var  data1 = data.GroupBy(a => new { a.PlanId })
            .Select(c => new PlanEntity
            {

                ActualNum =SqlFunc.AggregateSum(c.Num),
                ActualNum1 = SqlFunc.AggregateSum(c.ActualNum),
            });



            return await data1.ToListAsync();
        }
        public async Task<List<PlanEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<PlanEntity>> GetLookList(SoulPage<PlanEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }
        public async Task<List<PlanEntity>> GetLookListRun(SoulPage<PlanEntity> pagination, string keyword = "", string id = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false&&a.EnabledMark==1);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(id))
            {
                query = query.Where(a => a.Id == id);
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<PlanEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<PlanEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(PlanEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0];
                entity.MaterialCode = code.EnCode;
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                //此处需修改
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0];
                entity.MaterialCode = code.EnCode;
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }
        public async Task SubmitFormState(int state, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            { var ids = keyValue.Split(',');
                foreach (var item in ids)
                {
                    var t = await repository.Db.Updateable<PlanEntity>().SetColumns(p => new PlanEntity() {EnabledMark =state }).Where(it => it.Id == item).ExecuteCommandAsync();

                }
            }
        }
       
        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
