﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.MESManage;
using WaterCloud.Domain.BasicdataManagement;
using System.Numerics;
using WaterCloud.Domain.WMSManagement;

namespace WaterCloud.Service.MESManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-09 13:24
    /// 描 述：排产计划服务类
    /// </summary>
    public class PsplanService : DataFilterService<PsplanEntity>, IDenpendency
    {
        public PsplanService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<PsplanEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.SortCode).ToListAsync();
        }
       
        public async Task<List<PsplanEntity>> GetListIsNull(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.PlanId.Contains(keyword));
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<PsplanEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.SortCode).ToListAsync();
        }

        public async Task<List<PsplanEntity>> GetLookList(SoulPage<PsplanEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.OrderBy(a => a.StartDate).ToPageListAsync(pagination);
        }

        public async Task<PsplanEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<PsplanEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(PsplanEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                var m= await repository.Db.Queryable<ProductEntity>().Where(a => a.Id.Contains(entity.MaterialId)).ToListAsync();
                entity.MaterialName = m[0].ProductName;
                entity.MaterialCode = m[0].EnCode;
                var r = await repository.Db.Queryable<ProcessrouteheadEntity>().Where(a => a.Id.Contains(entity.ProcessRouteId)).ToListAsync();
                entity.ProcessRouteName = r[0].FullName;
                entity.ProcessRouteVersion = r[0].EnCode.ToInt();
                var l = await repository.Db.Queryable<LineEntity>().Where(a => a.Id.Contains(entity.LineId)).ToListAsync();
                entity.LineName = l[0].FullName;
                entity.LineCode = l[0].EnCode;



                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
