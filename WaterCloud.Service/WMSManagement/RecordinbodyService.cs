﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Domain.BasicdataManagement;
using NPOI.SS.Formula.Functions;
using MySqlX.XDevAPI.CRUD;
using RabbitMQ.Client;
using System.Text;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using WaterCloud.Domain.InfoManage;
using Serenity;
using WaterCloud.Domain.ContentManage;

namespace WaterCloud.Service.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-19 15:26
    /// 描 述：入库单明细服务类
    /// </summary>
    public class RecordinbodyService : DataFilterService<RecordinbodyEntity>, IDenpendency
    {
        public RabbitMqHelper rabbitMqHelper { get; set; }
        public RecordinbodyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<RecordinbodyEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<RecordinbodyEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<RecordinbodyEntity>> GetListIsNull(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.HeadId.Contains(keyword));
                
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<RecordinbodyEntity>> GetLookList(SoulPage<RecordinbodyEntity> pagination,string keyword = "",string hid="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(hid))
            {
                query= query.Where(a=>a.HeadId == hid);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }
        public async Task<List<RecordinbodyEntity>> GetLookListOut(SoulPage<RecordinbodyEntity> pagination, string keyword = "", string Warehouseid = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false&&a.State==true&&string.IsNullOrEmpty(a.OutRecordId));
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(Warehouseid))
            {
                query = query.Where(a => a.WarehouseId== Warehouseid);
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<List<RecordinbodyEntity>> GetLookListUp(SoulPage<RecordinbodyEntity> pagination)
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false&&a.State==false);
            
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<List<RecordinbodyEntity>> GetLookListUp(SoulPage<RecordinbodyEntity> pagination, string WarehouseId, string keyword)
        {
          

         var query =repository.IQueryable().Where(a => a.DeleteMark == false && a.State == true&&a.WarehouseId.Contains(WarehouseId)&&a.ProductCode.Contains(keyword)&&string.IsNullOrEmpty(a.OutRecordId));

            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
       
        public async Task<RecordinbodyEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
     

        public async Task<RecordinbodyEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }
        public async Task<List<RecordinbodyEntity>> GetListuped(string WarehouseId = "", string keyword = "")
        {
            var data = repository.IQueryable();
            data = data.Where(a => a.ProductCode == keyword && a.WarehouseId == WarehouseId && a.EnabledMark == true&&string.IsNullOrEmpty(a.OutRecordId));

            return await data.ToListAsync();
        }
        #endregion
        #region 提交数据
        public async Task SubmitForm(RecordinbodyEntity entity, string keyValue,string oldgid)
        {
            unitOfWork.CurrentBeginTrans();


            if (string.IsNullOrEmpty(keyValue))
            {
                var outlist = repository.Db.Queryable<RecordoutbodyEntity>().Where(a => a.EnCode== entity.EnCode && a.State==true).ToList();

                if (outlist.Count <= 0) { 
                //初始值添加
                entity.DeleteMark = false;
                entity.State = false;
                entity.Create();
                await repository.Insert(entity);
                if (!string.IsNullOrEmpty(entity.GoodsId)) {
                var t = repository.Db.Updateable<GoodsEntity>().SetColumns(p => new GoodsEntity() { State = true }).Where(it => it.Id == entity.GoodsId).ExecuteCommand();
                }
                }
            }
            else
            {

                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
                if (!string.IsNullOrEmpty(entity.GoodsId))
                {
                    if (!entity.GoodsId.Contains(oldgid)) {
                var t1 = repository.Db.Updateable<GoodsEntity>().SetColumns(p => new GoodsEntity() { State = false }).Where(it => it.Id ==oldgid).ExecuteCommand();
                var t = repository.Db.Updateable<GoodsEntity>().SetColumns(p => new GoodsEntity() { State = true }).Where(it => it.Id == entity.GoodsId).ExecuteCommand();
                    }
                }

            }

         
            
            unitOfWork.CurrentCommit();
        }
        public async Task Up(RecordinbodyEntity entity, string keyValue)

        {
            unitOfWork.CurrentBeginTrans();
            entity.AuditUserName = currentuser.UserName;
            entity.AuditUserId = currentuser.UserId;
            entity.AuditTime = DateTime.Now;
            entity.State = true;
           
            
            entity.Modify(keyValue);
            await repository.Update(entity);
          
            // AVG上架 插入消息队列  0 手工  1 AGV
            int sw = repository.Db.Queryable<WarehouseEntity>().Where(a => a.Id.Contains(entity.WarehouseId)).ToList()[0].StorageWay.ToInt();
            if (sw == 1)
            {
                //    //通过http发送消息
                if (GlobalContext.SystemConfig.RabbitMq.Enabled)
                {  entity.companyId=currentuser.CompanyId;  
                    entity.Tag = 0;  // 0 入库  1  出库
                    rabbitMqHelper.Publish(entity);
                }
            }
            unitOfWork.CurrentCommit();
            }
       
        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
