﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.WMSManagement;

namespace WaterCloud.Service.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-23 13:32
    /// 描 述：出库单服务类
    /// </summary>
    public class RecordoutheadService : DataFilterService<RecordoutheadEntity>, IDenpendency
    {
        public RecordoutheadService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<RecordoutheadEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.RecodNum.Contains(keyword)
                || a.OrderNum.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<RecordoutheadEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.RecodNum.Contains(keyword)
                || a.OrderNum.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }
        public async Task<List<RecordoutheadEntity>> GetLookListid(string id)
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            
                //此处需修改
                query = query.Where(a => a.Id==id);
            
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToListAsync();
        }
        public async Task<List<RecordoutheadEntity>> GetLookList(SoulPage<RecordoutheadEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.RecodNum.Contains(keyword)
                || a.OrderNum.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }

        public async Task<RecordoutheadEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<RecordoutheadEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(RecordoutheadEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
