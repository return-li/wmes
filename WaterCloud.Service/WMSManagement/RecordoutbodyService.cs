﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Domain.BasicdataManagement;
using NPOI.HPSF;

namespace WaterCloud.Service.WMSManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-08-23 13:34
    /// 描 述：出库单明细服务类
    /// </summary>
    public class RecordoutbodyService : DataFilterService<RecordoutbodyEntity>, IDenpendency
    {
        public RabbitMqHelper rabbitMqHelper { get; set; }
        public RecordoutbodyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<RecordoutbodyEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<RecordoutbodyEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

       
        public async Task<List<RecordoutbodyEntity>> GetListIsNull(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.HeadId.Contains(keyword));

            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<RecordoutbodyEntity>> GetLookList(SoulPage<RecordoutbodyEntity> pagination, string keyword = "", string hid = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(hid))
            {
                query = query.Where(a => a.HeadId == hid);
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<List<RecordoutbodyEntity>> GetLookListDown(SoulPage<RecordoutbodyEntity> pagination)
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false && a.State == false);

            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<RecordoutbodyEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<RecordoutbodyEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(string keyValue, string wid, string hid)
        {
            //初始值添加
            var ids = keyValue.Split(',');
            foreach (var item in ids)
            {
                unitOfWork.CurrentBeginTrans();
                var inentity = repository.Db.Queryable<RecordinbodyEntity>().Where(it => it.Id == item).ToList();
                RecordoutbodyEntity entity = new RecordoutbodyEntity();
                string id = Utils.GuId();
                entity.EnCode = inentity[0].EnCode;
                entity.Id = id;
                entity.HeadId = hid;
                entity.CreatorUserName = currentuser.UserName;
                entity.CreatorTime = DateTime.Now;
                entity.State = false;
                entity.DeleteMark = false;
                entity.WarehouseId = inentity[0].WarehouseId;
                entity.AreaId = inentity[0].AreaId;
                entity.ShelvesId = inentity[0].ShelvesId;
                entity.GoodsId = inentity[0].GoodsId;
                entity.ProductCode = inentity[0].ProductCode;
                entity.ProductName = inentity[0].ProductName;
                entity.Cinvstd = inentity[0].Cinvstd;
                entity.Batch = inentity[0].Batch;
                entity.Number = inentity[0].Number;
                entity.Number1 = inentity[0].Number1;
                entity.CinvmUnit = inentity[0].CinvmUnit;
                entity.CinvmUnit1 = inentity[0].CinvmUnit1;
               
                await repository.Insert(entity);
                var t = repository.Db.Updateable<RecordinbodyEntity>().SetColumns(p => new RecordinbodyEntity() { OutRecordId = id }).Where(it => it.Id == item).ExecuteCommand();
                //AGV下架 插入消息队列  0 手工  1 AGV
                int sw = repository.Db.Queryable<WarehouseEntity>().Where(a => a.Id.Contains(entity.WarehouseId)).ToList()[0].StorageWay.ToInt();
                if (sw == 1)
                {
                    //    //通过http发送消息
                    if (GlobalContext.SystemConfig.RabbitMq.Enabled)
                    { entity.companyId = currentuser.CompanyId;
                        entity.Tag = 1;  // 0 入库  1  出库
                        rabbitMqHelper.Publish(entity);
                    }
                }
                unitOfWork.CurrentCommit();
            }

        }
           
        public async Task Down(RecordoutbodyEntity entity, string keyValue,string GoodsId)

        {
            unitOfWork.CurrentBeginTrans();
            entity.AuditUserName = currentuser.UserName;
            entity.AuditUserId = currentuser.UserId;
            entity.AuditTime = DateTime.Now;
            entity.State = true;
            entity.Modify(keyValue);
            await repository.Update(entity);
            var t = repository.Db.Updateable<GoodsEntity>().SetColumns(p => new GoodsEntity() { State =false }).Where(it => it.Id == GoodsId).ExecuteCommand();
            unitOfWork.CurrentCommit();



        }
        public async Task DeleteForm(string keyValue)
        {
            unitOfWork.CurrentBeginTrans();
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
            var t = repository.Db.Updateable<RecordinbodyEntity>().SetColumns(p => new RecordinbodyEntity() { OutRecordId ="" }).Where(it => ids.Contains(it.OutRecordId.ToString())).ExecuteCommand();
            unitOfWork.CurrentCommit();
        }
        #endregion

    }
}
