﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.WMSManagement;
using WaterCloud.Domain.BasicdataManagement;
using NPOI.SS.Formula.Functions;
using MySqlX.XDevAPI.CRUD;
using RabbitMQ.Client;
using System.Text;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using WaterCloud.Domain.InfoManage;
using Serenity;
using WaterCloud.Domain.ContentManage;
using System.Collections;

namespace WaterCloud.Service.WMSManagement
{
    public class RePortService : DataFilterService<ReportEntity>, IDenpendency
    {
      
        public RePortService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<RecordinbodyEntity>> GetLookList(SoulPage<RecordinbodyEntity> pagination,string keyword = "" ,string Warehouse = "")
        {
            var data = repository.Db.Queryable<RecordinbodyEntity>().Where(a=>string.IsNullOrEmpty(a.OutRecordId));
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.ProductCode.Contains(keyword)
                || a.ProductName.Contains(keyword)
                || a.Cinvstd.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(Warehouse))
            {
                data = data.Where(b => b.WarehouseId == Warehouse);
            }
            data = data.GroupBy(a => new { a.WarehouseId, a.ProductCode, a.ProductName, a.Cinvstd, a.Batch })
            .Select(c => new RecordinbodyEntity
            {
                WarehouseName = SqlFunc.Subqueryable<WarehouseEntity>().Where(s => s.Id == c.WarehouseId).Select(s => s.WarehouseName),
                ProductCode = c.ProductCode,
                ProductName = c.ProductName,
                Cinvstd = c.Cinvstd,
                Batch = c.Batch,
                Number = SqlFunc.AggregateSum(c.Number),
                Number1 = SqlFunc.AggregateSum(c.Number1),
            });

            data = GetDataPrivilege("a", "", data);
            return await data.ToPageListAsync(pagination);
        }
        #endregion

    }
}
