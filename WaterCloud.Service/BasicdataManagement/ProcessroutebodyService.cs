﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;
using WaterCloud.Domain.WMSManagement;
using System.Collections;
using static iTextSharp.text.pdf.AcroFields;
using NPOI.SS.Formula.Functions;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-05 13:09
    /// 描 述：工艺路线记录服务类
    /// </summary>
    public class ProcessroutebodyService : DataFilterService<ProcessroutebodyEntity>, IDenpendency
    {
        public ProcessroutebodyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<ProcessroutebodyEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.LineCode.Contains(keyword)
                || a.LineName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ProcessroutebodyEntity>> GetListProcessRoute(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.HeadId.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ProcessroutebodyEntity>> GetListIsNull(string keyword = "")
        {
           var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a =>a.HeadId.Contains(keyword));
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ProcessroutebodyEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.LineCode.Contains(keyword)
                || a.LineName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<ProcessroutebodyEntity>> GetLookList(SoulPage<ProcessroutebodyEntity> pagination,string keyword = "",string hid="")
        {
            var query = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.LineCode.Contains(keyword)
                || a.LineName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(hid))
            {
                query= query.Where(a=>a.HeadId==hid);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }

        public async Task<ProcessroutebodyEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<ProcessroutebodyEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm( string keyValue,string hid)
        {    //最大排序值
             unitOfWork.CurrentBeginTrans();
            

            //初始值添加
            var ids = keyValue.Split(',');
            foreach (var item in ids)
            {
                var maxsoft= await repository.IQueryable().Where(a => a.HeadId == hid).MaxAsync(it => it.SortCode);
                var query = repository.Db.Queryable<ProcessEntity, LineEntity>((a, b) => new JoinQueryInfos(
                   JoinType.Left, a.LineId == b.Id && b.EnabledMark == true
                   ))
               .Select((a, b) => new ProcessEntity
               {
                   Id = a.Id.SelectAll(),
                   LineName = b.FullName,
                   LineNum = b.EnCode
               }).MergeTable();
                if (!string.IsNullOrEmpty(item))
                {
                    query = query.Where(a => a.Id.Contains(item)
                    || a.FullName.Contains(item));
                }
               var  inentity=  await query.ToListAsync();

                ProcessroutebodyEntity entity = new ProcessroutebodyEntity();
                string id = Utils.GuId();
                entity.LineCode = inentity[0].LineNum;
                entity.LineName = inentity[0].LineName;
                entity.LineId= inentity[0].LineId;
                entity.ProcessCode = inentity[0].EnCode;
                entity.ProcessName = inentity[0].FullName;
                entity.ProcessId = inentity[0].Id;
                entity.SortCode = maxsoft.ToInt() + 1;
                entity.Id = id;
                entity.HeadId = hid;
                entity.CreatorUserName = currentuser.UserName;
                entity.CreatorTime = DateTime.Now;
                entity.DeleteMark = false;
                await repository.Insert(entity);
       
                
            }
             unitOfWork.CurrentCommit();
        }
        public async Task Up(string keyValue,string hid)
        {    //最大排序值
           unitOfWork.CurrentBeginTrans();
            var maxsoft = await repository.IQueryable().Where(a => a.HeadId == hid).MaxAsync(it => it.SortCode);
            var sort = repository.Db.Queryable<ProcessroutebodyEntity>().Where(a => a.Id==keyValue&&a.HeadId==hid).ToList()[0].SortCode.ToInt();
            var nextid = repository.Db.Queryable<ProcessroutebodyEntity>().Where(a => a.SortCode == sort+1 && a.HeadId == hid).ToList()[0].Id;
            if (sort < maxsoft&&!string.IsNullOrEmpty(nextid))
            {
                int sort1 = sort + 1;
                var t = await repository.Db.Updateable<ProcessroutebodyEntity>().SetColumns(p => new ProcessroutebodyEntity() { SortCode = sort1 }).Where(it => it.Id == keyValue).ExecuteCommandAsync();
                var t1 = await repository.Db.Updateable<ProcessroutebodyEntity>().SetColumns(p1 => new ProcessroutebodyEntity() { SortCode = sort }).Where(it => it.Id==nextid).ExecuteCommandAsync();
            }
          unitOfWork.CurrentCommit();
        }
        public async Task Down(string keyValue, string hid)
        {    //最大排序值
           unitOfWork.CurrentBeginTrans();
         
            var sort =  repository.Db.Queryable<ProcessroutebodyEntity>().Where(a => a.Id == keyValue && a.HeadId == hid).ToList()[0].SortCode.ToInt();
            var nextid = repository.Db.Queryable<ProcessroutebodyEntity>().Where(a => a.SortCode == sort - 1 && a.HeadId == hid).ToList()[0].Id;
            if (sort > 1&&!string.IsNullOrEmpty(nextid))
            {
               int sort1 = sort - 1;
                var t = await repository.Db.Updateable<ProcessroutebodyEntity>().SetColumns(p => new ProcessroutebodyEntity() { SortCode = sort1 }).Where(it => it.Id == keyValue).ExecuteCommandAsync();

                 var t1 = await repository.Db.Updateable<ProcessroutebodyEntity>().SetColumns(p1 => new ProcessroutebodyEntity() { SortCode = sort }).Where(it => it.Id==nextid).ExecuteCommandAsync();
            }
            unitOfWork.CurrentCommit();
        }
        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
