﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-01-07 10:48
    /// 描 述：业务类型服务类
    /// </summary>
    public class BusinesstypesService : DataFilterService<BusinesstypesEntity>, IDenpendency
    {
        public BusinesstypesService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<BusinesstypesEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.BusinessTypeName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<BusinesstypesEntity>> GetListapi(string s )
        {
         
        
            return await repository.IQueryable().Where(a => a.Checked.Contains(s) || a.Input.Contains(s)).Where(a => a.DeleteMark == false && a.EnabledMark == true).ToListAsync();
    }
        public async Task<List<BusinesstypesEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.BusinessTypeName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public object ActionValidate(object action)
        {
            throw new NotImplementedException();
        }

        public async Task<List<BusinesstypesEntity>> GetLookList(SoulPage<BusinesstypesEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.BusinessTypeName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<BusinesstypesEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<BusinesstypesEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(BusinesstypesEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
