﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 13:58
    /// 描 述：存储区域服务类
    /// </summary>
    public class StorageAreaService : DataFilterService<StorageAreaEntity>, IDenpendency
    {
        public StorageAreaService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<StorageAreaEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.AreaName.Contains(keyword) || a.WarehouseId.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).Where(a => a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<StorageAreaEntity>> GetListarea(string keyword )
        {
            var data = repository.IQueryable();
           
                data = data.Where(a => a.WarehouseId==keyword);
           
            return await data.Where(a => a.DeleteMark == false).Where(a => a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<StorageAreaEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.AreaName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<StorageAreaEntity>> GetLookList(SoulPage<StorageAreaEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<StorageAreaEntity,WarehouseEntity>((a, b) => new JoinQueryInfos(
                  JoinType.Left, a.WarehouseId == b.Id && b.EnabledMark == true
                  ))
              .Select((a, b) => new StorageAreaEntity
              {
                  Id = a.Id.SelectAll(),
                  WarehouseName = b.WarehouseName,
              }).MergeTable();
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.AreaName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            query = query.Where(a => a.DeleteMark == false);
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<StorageAreaEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<StorageAreaEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(StorageAreaEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
