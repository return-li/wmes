﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-15 15:08
    /// 描 述：仓库类型服务类
    /// </summary>
    public class WarehousetypeService : DataFilterService<WarehousetypeEntity>, IDenpendency
    {
        public WarehousetypeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<WarehousetypeEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.WarehousetypeName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).Where(a => a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<WarehousetypeEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.WarehousetypeName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<WarehousetypeEntity>> GetLookList(SoulPage<WarehousetypeEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.WarehousetypeName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<WarehousetypeEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<WarehousetypeEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(WarehousetypeEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
