﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 13:20
    /// 描 述：托盘管理服务类
    /// </summary>
    public class PalletService : DataFilterService<PalletEntity>, IDenpendency
    {
        public PalletService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<PalletEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.palletName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<PalletEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.palletName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<PalletEntity>> GetLookList(SoulPage<PalletEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<PalletEntity, PallettypeEntity>((a, b) => new JoinQueryInfos(
                  JoinType.Left, a.pallettypeId == b.Id && b.EnabledMark == true
                  ))
              .Select((a, b) => new PalletEntity
              {
                  Id = a.Id.SelectAll(),
                  pallettypeName = b.pallettypeName,
              }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.palletName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = query.Where(a => a.DeleteMark == false);
            query = GetDataPrivilege("a","",query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<PalletEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<PalletEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task<string> SubmitForm(PalletEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                string r = "";
                var query = repository.IQueryable().Where(a => a.EnCode == entity.EnCode);
                if (query.ToList().Count == 0)
                {
                    entity.DeleteMark = false;
                    entity.Create();
                    await repository.Insert(entity);
                    return "ok";
                }
                else {

                    return "no";

                }
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
                return "ok";
            }
        }
        public async Task<string> SubmitFormBatch(PalletEntity entity)
        {
            string r = "";
            for (int i = 1; i <= Convert.ToInt32(entity.Num); i++) { 

                string bm = "P";
                if (entity.Num.ToString().Trim().Length == 1) {
                  bm=  DateTime.Now.ToString("yyyyMMddHHmmss") + "00" + i.ToString();
                }
                if (entity.Num.ToString().Trim().Length == 2)
                {
                    bm = DateTime.Now.ToString("yyyyMMddHHmmss") + "0" + i.ToString();
                }
                if (entity.Num.ToString().Trim().Length == 3)
                {
                    bm = DateTime.Now.ToString("yyyyMMddHHmmss") + i.ToString();
                }
                var query = repository.IQueryable().Where(a => a.EnCode == bm);
                if (query.ToList().Count == 0)
                {
                    entity.EnabledMark = true;
                    entity.DeleteMark = false;
                    entity.EnCode = bm;
                    entity.palletName = bm;
                    entity.SortCode = 1;
                    entity.Create();

                    await repository.Insert(entity);

                }
                else {

                    r = r +"/"+ bm;
                }
               
            }


            return r;
           
        }
        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
