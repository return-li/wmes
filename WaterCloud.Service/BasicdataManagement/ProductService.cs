﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-23 15:22
    /// 描 述：产品服务类
    /// </summary>
    public class ProductService : DataFilterService<ProductEntity>, IDenpendency
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<ProductEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.ProductName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ProductEntity>> GetListId(string keyword)
        {
            var data = repository.IQueryable();
            
                data = data.Where(a => a.EnCode.Equals(keyword) );
                return await data.Where(a => a.DeleteMark == false&& a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
           
            
        }
        public async Task<List<ProductEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.ProductName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<ProductEntity>> GetLookList(SoulPage<ProductEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<ProductEntity, ProductTypeEntity>((a, b) => new JoinQueryInfos(
               JoinType.Left, a.ProductTypeId == b.Id

               ))
            .Select((a, b) => new ProductEntity
            {
                Id = a.Id.SelectAll(),
                ProductTypeName = b.ProductTypeName,

            }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.ProductName.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(id))
            {
                query = query.Where(a => a.Id == id);
            }
            query = query.Where(a => a.DeleteMark == false);
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<ProductEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<ProductEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(ProductEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
