﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;
using Quartz.Util;
using Serenity;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-20 14:17
    /// 描 述：货位管理服务类
    /// </summary>
    public class GoodsService : DataFilterService<GoodsEntity>, IDenpendency
    {
        public GoodsService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<GoodsEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword) || a.ShelvesId.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<GoodsEntity>> GetListIsNull(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.ShelvesId==(keyword));
            }
            return await data.Where(a => a.DeleteMark == false && a.EnabledMark == true&&a.State==false).OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<GoodsEntity>> GetListlocation(string keyword )
        {
            var data = repository.IQueryable();
           
                data = data.Where(a => a.ShelvesId==keyword);
            
            return await data.Where(a => a.DeleteMark == false&& a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<GoodsEntity>> GetListlocationIsNull(string keyword)
        {
            var data = repository.IQueryable();

            data = data.Where(a => a.Id == keyword);

            return await data.Where(a => a.DeleteMark == false && a.EnabledMark == true&& a.State==false).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<GoodsEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<GoodsEntity>> GetLookList(SoulPage<GoodsEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<GoodsEntity, ShelvesEntity, WarehouseEntity, StorageAreaEntity>((a, b, c, d) => new JoinQueryInfos(
                JoinType.Left, a.ShelvesId == b.Id && b.EnabledMark == true,
                JoinType.Left, a.WarehouseId == c.Id && c.EnabledMark == true,
                JoinType.Left, a.AreaId == d.Id && d.EnabledMark == true
                ))
             .Select((a, b, c, d) => new GoodsEntity
             {
                 Id = a.Id.SelectAll(),
                 ShelvesName = b.ShelvesName,
                 WarehouseName = c.WarehouseName,
                 AreaName = d.AreaName
             }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.ShelvesName.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(id))
            {
                query = query.Where(a => a.Id == id);
            }
            query = query.Where(a => a.DeleteMark == false);
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<GoodsEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<GoodsEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }
        
        #region 提交数据
        public async Task SubmitForm(GoodsEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
