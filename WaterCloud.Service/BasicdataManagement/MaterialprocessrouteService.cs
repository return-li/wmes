﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-13 11:51
    /// 描 述：分配工艺路线服务类
    /// </summary>
    public class MaterialprocessrouteService : DataFilterService<MaterialprocessrouteEntity>, IDenpendency
    {
        public MaterialprocessrouteService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<MaterialprocessrouteEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.ProcessRouteName.Contains(keyword)
                || a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }
        public async Task<List<MaterialprocessrouteEntity>> GetListMaterialId(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.MaterialId.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id, OrderByType.Desc).ToListAsync();
        }
        public async Task<List<MaterialprocessrouteEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.ProcessRouteName.Contains(keyword)
                || a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<MaterialprocessrouteEntity>> GetLookList(SoulPage<MaterialprocessrouteEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.ProcessRouteName.Contains(keyword)
                || a.MaterialName.Contains(keyword)
                || a.MaterialCode.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }

        public async Task<MaterialprocessrouteEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<MaterialprocessrouteEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(MaterialprocessrouteEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0].EnCode;
                entity.MaterialCode = code;
                var v = repository.Db.Queryable<ProcessrouteheadEntity>().Where(a => a.Id == entity.ProcessRouteId).ToList()[0].EnCode;
                entity.ProcessRouteVersion = v.ToInt();
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                //此处需修改
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0].EnCode;
                entity.MaterialCode = code;
                var v = repository.Db.Queryable<ProcessrouteheadEntity>().Where(a => a.Id == entity.ProcessRouteId).ToList()[0].EnCode;
                entity.ProcessRouteVersion = v.ToInt();
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
