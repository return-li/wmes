﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2021-12-16 15:27
    /// 描 述：货架管理服务类
    /// </summary>
    public class ShelvesService : DataFilterService<ShelvesEntity>, IDenpendency
    {
        public ShelvesService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<ShelvesEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.ShelvesName.Contains(keyword) || a.AreaId.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).Where(a => a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ShelvesEntity>> GetListShelves(string keyword )
        {
            var data = repository.IQueryable();
          
                data = data.Where(a =>  a.AreaId.Contains(keyword));
           
            return await data.Where(a => a.DeleteMark == false).Where(a => a.EnabledMark == true).OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<ShelvesEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.ShelvesName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id).ToListAsync();
        }

        public async Task<List<ShelvesEntity>> GetLookList(SoulPage<ShelvesEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<ShelvesEntity, ShelvestypeEntity, WarehouseEntity, StorageAreaEntity> ((a, b,c,d) => new JoinQueryInfos(
                 JoinType.Left, a.ShelvestypeId == b.Id && b.EnabledMark == true  ,
                 JoinType.Left, a.WarehouseId == c.Id && c.EnabledMark == true, 
                 JoinType.Left, a.AreaId == d.Id && d.EnabledMark == true
                 ))
             .Select((a, b,c,d) => new ShelvesEntity
             {
                 Id = a.Id.SelectAll(),
                 ShelvestypeName = b.ShelvestypeName,
                 WarehouseName = c.WarehouseName,
                 AreaName=d.AreaName
             }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.ShelvesName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            query = query.Where(a => a.DeleteMark == false);
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return await query.ToPageListAsync(pagination);
        }

        public async Task<ShelvesEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<ShelvesEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(ShelvesEntity entity, string keyValue)
        {
            if (string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);

            }
            else
            {
                //此处需修改
                entity.Modify(keyValue);
                await repository.Update(entity);

            }

        }


        //生成货位
            public async Task SubmitFormGoods(string keyValue)
        {
            var ids = keyValue.Split(',');
            for (int n = 0; n < ids.Length; n++) { 

            var entity = await repository.FindEntity(ids[n]);

            string wcode = repository.Db.Queryable<WarehouseEntity>().Where(a => a.Id == entity.WarehouseId).First().EnCode;
            string acode = repository.Db.Queryable<StorageAreaEntity>().Where(a => a.Id == entity.AreaId).First().EnCode;
            var dataList = new List<GoodsEntity>();
        
            int floornum = Convert.ToInt32(entity.FloorNum);
            int num = Convert.ToInt32(entity.Num);
            string s = "", s1 = "";
            for (int i = 1; i <= floornum; i++)
            {
                if (i.ToString().Length == 1)
                {

                    s = "0" + i.ToString();
                }
               
                for (int j = 1; j <= num; j++)
                {

                    if (j.ToString().Length == 1)
                    {

                        s1 = "00" + j.ToString();
                    }
                    if (j.ToString().Length == 2)
                    {

                        s1 = "0" + j.ToString();
                    }


                    GoodsEntity item = new GoodsEntity();
                    item.Create();
                    item.Id = Utils.GuId();
                    item.EnCode = wcode + acode + entity.EnCode + s + s1;
                    item.WarehouseId = entity.WarehouseId;
                    item.AreaId = entity.AreaId;
                    item.ShelvesId = entity.Id;
                    item.DeleteMark = false;
                    item.EnabledMark = true;
                    item.State = false;
                    item.CreatorTime = DateTime.Now;
                    item.CreatorUserId = currentuser.UserId;
                    item.CreatorUserName= currentuser.UserName;
                    item.SortCode = 0;
                    dataList.Add(item);
                    
                }

            }
            unitOfWork.CurrentBeginTrans();
          
            await repository.Db.Deleteable<GoodsEntity>().Where(a => a.ShelvesId==entity.Id).ExecuteCommandAsync();
            await repository.Db.Insertable(dataList).ExecuteCommandAsync();
            unitOfWork.CurrentCommit();
         }
        }

        public async Task DeleteForm(string keyValue)
        { var ids = keyValue.Split(',');
            var g= repository.Db.Queryable<GoodsEntity>().Where(a => ids.Contains(a.ShelvesId.ToString()));
            if (g.ToList().Count>0)
            {

                throw new Exception("已存在货位，不能删除!");

            }
            else { await repository.Delete(a => ids.Contains(a.Id.ToString()));}
           
            
        }
        #endregion

    }
}
