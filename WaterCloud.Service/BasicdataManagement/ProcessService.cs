﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-02 15:29
    /// 描 述：工序服务类
    /// </summary>
    public class ProcessService : DataFilterService<ProcessEntity>, IDenpendency
    {
        public ProcessService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<ProcessEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.EnCode.Contains(keyword)
                || a.FullName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<ProcessEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.FullName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<ProcessEntity>> GetLookList(SoulPage<ProcessEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.Db.Queryable<ProcessEntity, LineEntity>((a, b) => new JoinQueryInfos(
                   JoinType.Left, a.LineId == b.Id && b.EnabledMark == true
                   ))
               .Select((a, b) => new ProcessEntity
               {
                   Id = a.Id.SelectAll(),
                   LineName = b.FullName,
                   LineNum = b.EnCode
               }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.FullName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }
        public async Task<List<ProcessEntity>> GetLookListProcessroute(SoulPage<ProcessEntity> pagination, string keyword = "", string LineId = "", string hid = "")
        {
            var query1 = repository.Db.Queryable<ProcessroutebodyEntity>().Where(a => a.HeadId==hid).ToList();


           var ids=query1.Select(a=>a.ProcessId).ToArray();
        

           var query = repository.Db.Queryable<ProcessEntity, LineEntity>((a, b) => new JoinQueryInfos(
                   JoinType.Left, a.LineId == b.Id && b.EnabledMark == true
                   ))
               .Select((a, b) => new ProcessEntity
               {
                   Id = a.Id.SelectAll(),
                   LineName = b.FullName,
                   LineNum = b.EnCode
               }).MergeTable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.EnCode.Contains(keyword)
                || a.FullName.Contains(keyword));
            }
            if (!string.IsNullOrEmpty(LineId))
            {
                query = query.Where(a => a.LineId == LineId);
            }
            query = query.Where(a =>!ids.Contains(a.Id));
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.ToPageListAsync(pagination);
        }
        public async Task<ProcessEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<ProcessEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(ProcessEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                    //初始值添加


                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                    //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
