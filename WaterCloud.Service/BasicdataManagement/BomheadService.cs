﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-08 14:14
    /// 描 述：物料BOM服务类
    /// </summary>
    public class BomheadService : DataFilterService<BomheadEntity>, IDenpendency
    {
        public BomheadService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<BomheadEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }
        public async Task<List<BomheadEntity>> GetListbb(string mid = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(mid))
            {
                data = data.Where(a => a.MaterialId.Contains(mid));
            }
            return await data.Where(a => a.EnabledMark == true).OrderBy(a => a.Id, OrderByType.Desc).ToListAsync();
        }
        public async Task<List<BomheadEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<BomheadEntity>> GetLookList(SoulPage<BomheadEntity> pagination,string keyword = "",string id="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(id))
            {
                query= query.Where(a=>a.Id==id);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }

        public async Task<BomheadEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<BomheadEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(BomheadEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0].EnCode;
                entity.MaterialCode = code;
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0].EnCode;
                entity.MaterialCode = code;
                //此处需修改
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
