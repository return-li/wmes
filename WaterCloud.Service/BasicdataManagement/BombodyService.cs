﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using WaterCloud.Code;
using SqlSugar;
using WaterCloud.DataBase;
using WaterCloud.Domain.BasicdataManagement;

namespace WaterCloud.Service.BasicdataManagement
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2022-09-08 14:16
    /// 描 述：BOM记录服务类
    /// </summary>
    public class BombodyService : DataFilterService<BombodyEntity>, IDenpendency
    {
        public BombodyService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #region 获取数据
        public async Task<List<BombodyEntity>> GetList(string keyword = "")
        {
            var data = repository.IQueryable();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            return await data.Where(a => a.DeleteMark == false).OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }
        public async Task<List<BombodyEntity>> GetListIsNull(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.HeadId.Contains(keyword));
            }
            //权限过滤
            query = GetDataPrivilege("a", "", query);
            return await query.OrderBy(a => a.Id).ToListAsync();
        }
        public async Task<List<BombodyEntity>> GetLookList(string keyword = "")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                //此处需修改
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
             //权限过滤
             query = GetDataPrivilege("a", "", query);
             return await query.OrderBy(a => a.Id , OrderByType.Desc).ToListAsync();
        }

        public async Task<List<BombodyEntity>> GetLookList(SoulPage<BombodyEntity> pagination,string keyword = "",string hid="")
        {
            var query = repository.IQueryable().Where(a => a.DeleteMark == false);
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.MaterialCode.Contains(keyword)
                || a.MaterialName.Contains(keyword));
            }
            if(!string.IsNullOrEmpty(hid))
            {
                query= query.Where(a=>a.HeadId==hid);
            }
            //权限过滤
            query = GetDataPrivilege("a","",query);
            return  await query.ToPageListAsync(pagination);
        }

        public async Task<BombodyEntity> GetForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return data;
        }
        #endregion

        public async Task<BombodyEntity> GetLookForm(string keyValue)
        {
            var data = await repository.FindEntity(keyValue);
            return GetFieldsFilterData(data);
        }

        #region 提交数据
        public async Task SubmitForm(BombodyEntity entity, string keyValue)
        {
            if(string.IsNullOrEmpty(keyValue))
            {
                //初始值添加
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0];
                entity.MaterialCode = code.EnCode;
                entity.Unit = code.CinvmUnit;
                entity.DeleteMark = false;
                entity.Create();
                await repository.Insert(entity);
            }
            else
            {
                //此处需修改
                var code = repository.Db.Queryable<ProductEntity>().Where(a => a.Id == entity.MaterialId).ToList()[0];
                entity.MaterialCode = code.EnCode;
                entity.Unit = code.CinvmUnit;
                entity.Modify(keyValue); 
                await repository.Update(entity);
            }
        }

        public async Task DeleteForm(string keyValue)
        {
            var ids = keyValue.Split(',');
            await repository.Delete(a => ids.Contains(a.Id.ToString()));
        }
        #endregion

    }
}
